﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Caching;
using System.Threading.Tasks;

namespace CachingDemo.Core
{
    public class SkillsApiRepository : ISkillsRepository
    {
        private const string CacheKey_GetData = "SkillsApiRepository_GetData";
        private const string CacheKey_GetJsonData = "SkillsApiRepository_GetJsonData";
        private const string Url = "https://api.tracker-rms.com/WebAPI/GetWebReport.aspx?{insert-here}";
        private ObjectCache _cache = MemoryCache.Default;

        public async Task<IEnumerable<Skill>> GetData()
        {
            var result = Enumerable.Empty<Skill>();
            var resultJson = await this.GetJsonData();

            if (resultJson != null)
            {
                result = JsonConvert.DeserializeObject<IEnumerable<Skill>>(resultJson);
            }

            if (result != null && result.Any())
            {
                this._cache.Add(CacheKey_GetData, result, new CacheItemPolicy()
                {
                    AbsoluteExpiration = DateTime.Now.AddHours(24)
                });
            }

            return result;
        }

        public async Task<IEnumerable<Skill>> GetData_Cached()
        {
            var result = this._cache[CacheKey_GetData] as IEnumerable<Skill>;

            if (result == null)
            {
                result = await this.GetData();
            }

            return result;
        }

        public async Task<string> GetJsonData()
        {
            var result = default(string);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("user-agent", "Mozilla/4.0 {insert-here}");
                var response = await client.GetAsync(SkillsApiRepository.Url);

                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();

                    if (result != null)
                    {
                        this._cache.Add(CacheKey_GetJsonData, result, new CacheItemPolicy()
                        {
                            AbsoluteExpiration = DateTime.Now.AddHours(24)
                        });
                    }
                }
                else
                {
                    this._cache.Remove(CacheKey_GetJsonData);
                    var error = await response.Content.ReadAsStringAsync();
                    throw new Exception(error);
                }
            }

            return result;
        }

        public async Task<string> GetJsonData_Cached()
        {
            var result = this._cache[CacheKey_GetJsonData] as string;

            if (result == null)
            {
                result = await this.GetJsonData();
            }

            return result;
        }
    }
}