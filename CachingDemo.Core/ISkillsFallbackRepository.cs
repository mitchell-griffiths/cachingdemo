﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CachingDemo.Core
{
    public interface ISkillsFallbackRepository : ISkillsRepository
    {
        Task<IEnumerable<Skill>> GetData_Cached(CacheSource cacheSource);

        Task<string> GetJsonData_Cached(CacheSource cacheSource);
    }
}