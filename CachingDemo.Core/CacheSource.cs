﻿namespace CachingDemo.Core
{
    public enum CacheSource
    {
        None = 0,
        This,
        Fallback
    }
}