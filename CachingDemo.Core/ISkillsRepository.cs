﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CachingDemo.Core
{
    public interface ISkillsRepository
    {
        Task<IEnumerable<Skill>> GetData();

        Task<IEnumerable<Skill>> GetData_Cached();

        Task<string> GetJsonData();

        Task<string> GetJsonData_Cached();
    }
}