﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;

namespace CachingDemo.Core
{
    public class SkillsDatabaseRepository : ISkillsFallbackRepository
    {
        private const string CacheKey_GetData = "SkillsDatabaseRepository_GetData";
        private const string CacheKey_GetJsonData = "SkillsDatabaseRepository_GetJsonData";
        private ObjectCache _cache = MemoryCache.Default;
        private ISkillsRepository _fallbackRepository;

        public SkillsDatabaseRepository(ISkillsRepository fallbackRepository)
        {
            this._fallbackRepository = fallbackRepository ?? throw new ArgumentNullException(nameof(fallbackRepository));
        }

        public async Task<IEnumerable<Skill>> GetData()
        {
            var result = await this.GetDataFromDatabase();

            if (result == null || !result.Any())
            {
                result = await this._fallbackRepository.GetData_Cached();
                this.Insert(result);
            }

            if (result != null && result.Any())
            {
                this._cache.Add(CacheKey_GetData, result, new CacheItemPolicy()
                {
                    AbsoluteExpiration = DateTime.Now.AddHours(24)
                });
            }

            return result;
        }

        public async Task<IEnumerable<Skill>> GetData_Cached()
        {
            var result = this._cache[CacheKey_GetData] as IEnumerable<Skill>;

            if (result == null)
            {
                result = await this.GetData();
            }

            return result;
        }

        public async Task<IEnumerable<Skill>> GetData_Cached(CacheSource cacheSource)
        {
            var result = Enumerable.Empty<Skill>();

            switch (cacheSource)
            {
                case CacheSource.This:
                    result = await this.GetData_Cached();
                    break;

                case CacheSource.Fallback:
                    this._cache.Remove(CacheKey_GetData);
                    result = await this._fallbackRepository.GetData_Cached();
                    break;

                case CacheSource.None:
                default:
                    result = await _fallbackRepository.GetData();
                    break;
            }

            return result;
        }

        public async Task<string> GetJsonData()
        {
            var result = JsonConvert.SerializeObject(await this.GetData());

            if (result != null)
            {
                this._cache.Add(CacheKey_GetJsonData, result, new CacheItemPolicy()
                {
                    AbsoluteExpiration = DateTime.Now.AddHours(24)
                });
            }

            return result;
        }

        public async Task<string> GetJsonData_Cached()
        {
            var result = this._cache[CacheKey_GetJsonData] as string;

            if (result == null)
            {
                result = await this.GetJsonData();
            }

            return result;
        }

        public async Task<string> GetJsonData_Cached(CacheSource cacheSource)
        {
            var result = default(string);

            switch (cacheSource)
            {
                case CacheSource.This:
                    result = await this.GetJsonData_Cached();
                    break;

                case CacheSource.Fallback:
                    this._cache.Remove(CacheKey_GetData);
                    result = await this._fallbackRepository.GetJsonData_Cached();
                    break;

                case CacheSource.None:
                default:
                    result = await _fallbackRepository.GetJsonData();
                    break;
            }

            return result;
        }

        private async Task<IEnumerable<Skill>> GetDataFromDatabase()
        {
            // TODO:
            return new List<Skill>()
            {
                new Skill(),
                new Skill(),
                new Skill(),
                new Skill(),
                new Skill(),
                new Skill()
            };
        }

        private void Insert(IEnumerable<Skill> result)
        {
            // TODO:
        }
    }
}