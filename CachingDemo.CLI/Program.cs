﻿using CachingDemo.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CachingDemo.CLI
{
    internal class Program
    {
        private ISkillsRepository _skillsRepository;

        public Program()
        {
            this._skillsRepository = new SkillsDatabaseRepository(new SkillsApiRepository());
        }

        private static void Main(string[] args)
        {
            Task.Run(new Program().GetSkills).Wait();
        }

        private bool GetCacheBustParameter()
        {
            return false;
            // TODO:
            ////return this.Request.QueryString["cb"] == "1";
        }

        private async Task<IEnumerable<Skill>> GetSkills()
        {
            var result = Enumerable.Empty<Skill>();
            var cacheBust = this.GetCacheBustParameter();

            if (cacheBust)
            {
                result = await this._skillsRepository.GetData();
            }
            else
            {
                result = await this._skillsRepository.GetData_Cached();
            }

            return result;
        }
    }
}